#include "platform.h"

#if defined(STM32_LOWPOWER)
    #include <STM32LowPower.h>
#endif

#if defined(ARDUINO_ARCH_STM32) && ! defined(DEBUGPRINT) && defined(USE_WDT)
    #include <IWatchdog.h>
#endif


void checkWatchdog()
{
    #if defined(ARDUINO_ARCH_STM32) && ! defined(DEBUGPRINT) && defined(USE_WDT)
        if (IWatchdog.isReset())
        {
            IWatchdog.clearReset();
            blinkCode(ERROR_CODE_WATCHDOG);
        }
    #endif    
}


void enableWatchdog()
{
    #if defined(ARDUINO_ARCH_STM32) && ! defined(DEBUGPRINT) && defined(USE_WDT)
        IWatchdog.begin(WATCHDOG_TIMEOUT);
    #endif
}

void resetWatchdog()
{
    #if defined(ARDUINO_ARCH_STM32) && ! defined(DEBUGPRINT) && defined(USE_WDT)
        IWatchdog.reload();
    #endif
}

void initPowerManager()
{
    #ifdef ARDUINO_ARCH_SAMD
        zpmRTCInit();
    #endif
    
    #ifdef ARDUINO_ARCH_STM32
        #if defined(STM32_LOWPOWER)
        LowPower.begin();
        #endif 
    #endif

}

void nap()
{
    #ifdef ARDUINO_ARCH_SAMD
        uint32_t now = zpmRTCGetClock();
        zpmRTCInterruptAt(now + 1, NULL);
        zpmSleep(); // kills the USB interface, not good for debugging
        //zpmPlayPossum(); // keeps USB alive, good for debugging
    #endif

    #ifdef ARDUINO_ARCH_STM32
        
        #if defined(STM32_LOWPOWER)
        
        // lower than 8ms causes issues. 
        // Timer does not trigger wake-up. 
        // Probably because the timer has already expired by the time it is set. Using 10ms as a safety margin. In debug mode, this may still cause issues
        // Currently disabled (compiler flag STM32_LOWPOWER) as it appears to conflict with the KNX stack. Needs debugging.



        //LowPower.sleep(10);  
        LowPower.idle(10);
        #else
            __WFI();
        #endif
    #endif
}

void blinkCode(uint8_t code)
{
    pinMode(AUTO_LED, OUTPUT);
    
    // Switch LED off
    digitalWrite(AUTO_LED, !LED_ACTIVE);
    
    for (size_t i = 0; i < code; i++)
    {
        delay(200);
        digitalWrite(AUTO_LED, LED_ACTIVE);
        delay(200);
        digitalWrite(AUTO_LED, !LED_ACTIVE);
    }

}