#pragma once


#include "KNXDevice.h"
#include <SimpleTimer.h>
#include <KNXTrace.h>


#include <OneWire.h>
#include <DallasTemperature.h>

// controls LED blink frequency in case of sensor error
#define DEFAULT_ERROR_INTERVAL 1000

#define MAX_TIME_SINCE_LAST_CHANGE 60*1000 // time in millis (60 seconds)


class TemperatureSensor: public IKNXDevice
{

    private:
        String _name;
        GroupObject& _temp;
        

        OneWire* _oneWire;
        DallasTemperature* _sensor;

        SimpleTimer _timerStatusUpdate;
        SimpleTimer _timerError;
        SimpleTimer _timerConversion;

        float _curTemp;

        bool _paramPeriodic;
        uint32_t _paramInterval; // runtime parameter in milliseconds
        
        SimpleTimer _lastUpdate;

        KNXTrace *_trace = NULL;
            


    public:
        /**
         * @brief Construct a new temperature sensor object
         * 
         * @param name      Friendly name of the object
         * @param goTemp      Measured temperature  [OUT]
         * @param paramInterval Paramter controlling the sending interval (in 1s) for periodic updates
         * @param trace     debug tracer (optional)
         */
        TemperatureSensor(String name, GroupObject& goTemp, uint32_t paramInterval, KNXTrace *trace = NULL);
        ~TemperatureSensor();
    /**
     * @brief Helper functions
     * 
     */
    public: 
        virtual String ToString();
        virtual void UpdateState();
        virtual bool IsHealthy();

        virtual const char* GetName() {return _name.c_str();};


    /**
     * @brief Event handler
     * 
     */
    protected:       

        /**
         * @brief Update the internal temperature difference
         * 
         * @param temp - new temperature measurement
         */
        void setTemp(float temp);
        

        /**
         * @brief Periodically sends current temperature. If enabled by ETS param.
         * 
         */
        void statusUpdateTemperature();

        /**
         * @brief set trace message
         * 
         * @param msg 
         */
        void trace(const char* msg)
        {
            if (_trace)
                _trace->SetMsg(msg);
        }



};









