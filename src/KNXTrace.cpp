#include "KNXTrace.h"


// for std::function, std::bind
#include <functional>

KNXTrace::KNXTrace(String name, GroupObject&  goTrace, GroupObject& goMsg)
        : _name(name), _trace(goTrace), _msg(goMsg)
{
   
    _msg.dataPointType(DPT_String_ASCII);
    _trace.dataPointType(DPT_Switch);
    _trace.callback(std::bind(&KNXTrace::onEnable, this, std::placeholders::_1));

    // initialize
    _enabled = false;
    _curMsg[0] = '\0';

    _timerStatusUpdate.setInterval(DEFAULT_ERROR_INTERVAL);
    _timerStatusUpdate.reset();

    
    // force message out upon boot
    _msg.value("INIT");
    
    // locate devices on the bus
    DBGLN("KNXTrace initialized");
    
    
};

KNXTrace::~KNXTrace()
{

}

String KNXTrace::ToString()
{
    String s = "KNXTrace:\t";
    s += _name;

    s += "\n_enabled\t";
    s += _enabled;

    s += "\n_curMsg\t";
    s += _curMsg;

    
    return s;
};

void KNXTrace::onEnable(GroupObject& o)
{
    DBG(GetName());
    DBGLN("--KNXTrace: Trace Enable received");

    bool val = o.value(); 
    
    if (val == 0) // OFF
    {
        DBGLN("\tOFF");
        _enabled = false;
    }   
    else
    {
        DBGLN("\t=ON");
        _enabled = true;
    }


};

void KNXTrace::statusUpdateMessage()
{
    DBG(GetName());
    DBG("--KNXTrace: Sending latest message: ");
    DBGLN(_msg);

    if (_enabled)
        _msg.value(_curMsg);
    
    // reset timer    
    _timerStatusUpdate.reset();

};

bool KNXTrace::IsHealthy()
{
    return true;
}


void KNXTrace::SetMsg(const char* msg)
{
    size_t len = strlen(msg) > MAX_LEN ? MAX_LEN : strlen(msg);
    memset(_curMsg, 0, MAX_LEN);
    strncpy(_curMsg, msg, len);
   
}

void KNXTrace::UpdateState()
{
  
    // now, we handle the time-based events if periodic reporting is enabled
    if (_timerStatusUpdate.isReady())
    {
        statusUpdateMessage();
    }
    
};