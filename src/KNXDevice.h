#pragma once

#include "platform.h"

#include <WString.h>
#include <knx.h>


class IKNXDevice
{ 
    public:
    virtual ~IKNXDevice() {};

    public:
        /**
         * @brief Pretty print the object
         * 
         * @return String 
         */
        virtual String ToString() = 0;

        /**
         * @brief Get the Name object
         * 
         * @return const char* 
         */
        virtual const char* GetName() = 0;

        /**
         * @brief Update internal state and trigger update of Group Objects as required. 
         * 
         * ATTN: Must be called periodically in the program main loop
         * 
         */
        virtual void UpdateState() {/*nothing*/};
    
        /**
         * @brief Check health of the KNX device
         * 
         * @return true - iff healthy
         * @return false 
         */
        virtual bool IsHealthy() = 0;

};