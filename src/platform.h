
#pragma once


/**
 * @file platform.h
 * @author rueckix
 * @brief Contains all platform dependent code
 * @version 0.1
 * @date 2021-01-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */



#include <Arduino.h>


#include "debug.h"

#pragma once
#if defined(ARDUINO_ARCH_STM32)


// Data sheet
// data sheet https://www.st.com/resource/en/datasheet/stm32f401cb.pdf

// Power consumption
// <10 mA running, if LED (AUTO indicator) is connected via 10kOhm
// 26 mA for the first 10 seconds
//

// knx data types: https://www.knx.org/wAssets/docs/downloads/Certification/Interworking-Datapoint-types/03_07_02-Datapoint-Types-v02.02.01-AS.pdf



    #define pin_size_t uint32_t

    #define DEBUG_SERIAL Serial

    // time to take sleep nap [ms]
    // #define STM32_NAP_TIME 1 

    // PINS on STM32

    // define prog button if there is no on-board BTN
    #if USER_BTN==PNUM_NOT_DEFINED
        #define BTN_PROG_PIN PC15
    #else
        #define BTN_PROG_PIN USER_BTN // =PA0
    #endif
    #define LED_PROG LED_BUILTIN //=PC13
    #define LED_PROG_ACTIVE LOW
    #define LED_ACTIVE HIGH


    #define WATCHDOG_TIMEOUT IWDG_TIMEOUT_MAX // ~32 seconds

    // 5V Tolerance on WeAct Blackpill V2
    // All pins except PA0 [A0] and PB5 [B5]
    // No isolator (e.g., ADUM) needed for connecting to Siemens BCU, unless we required more than 20-50mA. Otherwise, fast rate isolator is required
    

    // Debug Serial Port Serial=Serial1
    // PIN_SERIAL_RX           PA10
    // PIN_SERIAL_TX           PA9

 
    // KNX UART uses Serial2
    // UART RX                  PA3 // Connected to ADUM VOB
    // UART TX                  PA2 // Connected to ADUM VIA

    #define AUTO_LED            PA1
    
    // OneWire connection for differential measurement (2 sensors)
    #define ONE_WIRE_BUS_DIFF        PB5

    // OneWire connection for absolute measurement (separate bus, 1 sensor)
    #define ONE_WIRE_BUS_ABS        PA10

    /**
     * @brief ATTN
     * PC13 - PC14 - PC15 MUST not be used as a source to drive a load
     * They can sink up to 3 mA.
     * 
     */
    
 
#elif defined(__AVR_ATmega328P__)
    #define pin_size_t uint32_t

    #define DEBUG_SERIAL Serial

    // time to take sleep nap [ms]
    // #define STM32_NAP_TIME 1 

    // PINS on STM32
    #define BTN_PROG_PIN USER_BTN
    #define LED_PROG LED_BUILTIN //=D13
    #define LED_PROG_ACTIVE LOW


    // 5V Tolerance on WeAct Blackpill V2
    // All pins except PA0 [A0] and PB5 [B5]
    // No isolator (e.g., ADUM) needed for connecting to Siemens BCU, unless we required more than 20-50mA. Otherwise, fast rate isolator is required

    // Debug Serial Port Serial=Serial1
    // PIN_SERIAL_RX           PA10
    // PIN_SERIAL_TX           PA9

    
    // KNX UART uses Serial2
    // UART RX                  PA3 // Connected to ADUM VOB
    // UART TX                  PA2 // Connected to ADUM VIA

    #define ONE_WIRE_BUS        PB5
    #define AUTO_LED        PC14

#else
#error "Device not supported (yet)"
#endif

/**
 * @brief Initialize power/sleep manager
 * 
 */
void initPowerManager();
/**
 * @brief Function invokes deep sleep followed by an interrupt after approximately 1 ms. Significantly reduces the power draw from 15mA to 3mA at the expense of a 1ms delay.
 * 
 */
void nap();


/**
 * @brief Enable Watchdog timer
 * 
 */
void enableWatchdog();

/**
 * @brief Reset Watchdog Timer
 * 
 */
void resetWatchdog();


/**
 * @brief Check if we are returning from a watchdog reset. Reset the flag and issues a blink code
 * 
 */
void checkWatchdog();

#define ERROR_CODE_START            3
#define ERROR_CODE_NOT_CONFIGURED   5
#define ERROR_CODE_BAU              7
#define ERROR_CODE_DEVICE_HEALTH    9  
#define ERROR_CODE_WATCHDOG         11


/**
 * @brief Send a blink code using the internal LED
 * 
 * @param code - nubmer of blinks
 */
void blinkCode(uint8_t code);

