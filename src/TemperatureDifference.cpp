#include "TemperatureDifference.h"


// for std::function, std::bind
#include <functional>




TemperatureDifference::TemperatureDifference(String name, GroupObject& goAuto, GroupObject&  goPump, GroupObject&  goTempDiff, GroupObject&  goT1, GroupObject&  goT2, uint8_t paramPeriodic, uint32_t paramInterval, uint32_t paramDelta, uint32_t paramMaxDelta,  uint32_t paramControlInterval, KNXTrace* trace)
        : _name(name), _auto(goAuto), _pump(goPump), _tdiff(goTempDiff), _t1(goT1), _t2(goT2), _trace(trace), _oneWire(new OneWire(ONE_WIRE_BUS_DIFF)), _sensors(new DallasTemperature(_oneWire))
{
   
    _auto.dataPointType(DPT_Switch);
    _tdiff.dataPointType(DPT_Value_Tempd);
    _pump.dataPointType(DPT_Switch);
    _t1.dataPointType(DPT_Value_Temp);
    _t2.dataPointType(DPT_Value_Temp);

    #ifdef PERSIST_TO_EEPROM
    // try to read EEPROM
    EEPROM.get(EEPROM_BEGIN, _eepromRecord);
    
    if (_eepromRecord.magic != MAGICCODE)
    {
        DBGLN("TemperatureDifference::TemperatureDifference - EEPROM not initialized");
        _eepromRecord.auto_flag = false;
        _eepromRecord.magic = MAGICCODE;
        EEPROM.put(EEPROM_BEGIN, _eepromRecord);
    }
    _autoControl = _eepromRecord.auto_flag;
    #else
    _autoControl = false;
    #endif
    
    // initialize
    
    _auto.value(_autoControl);
    _pump.value(false);
    _curTempDiff = _curTemp1 = _curTemp2 = 0.0f;
    _tdiff.value(_curTempDiff);
    _t1.value(_curTemp1);
    _t2.value(_curTemp2);
    
    // initialize LED
    pinMode(AUTO_LED, OUTPUT);
    digitalWrite(AUTO_LED, LOW);


    _auto.callback(std::bind(&TemperatureDifference::onAuto, this, std::placeholders::_1));
    
    _paramPeriodic = (paramPeriodic == 1); // convert to bool
    _paramInterval = paramInterval * 1000; //convert to ms
    _paramDelta = paramDelta;
    _paramMaxDelta = paramMaxDelta;

    _timerStatusUpdate.setInterval(_paramInterval);
    _timerStatusUpdate.reset();

    _timerError.setInterval(DEFAULT_ERROR_INTERVAL); // error blink code at 1 Hz
    _timerError.reset();


    _paramControlInterval = paramControlInterval * 1000; // convert to ms
    _timerControl.setInterval(_paramControlInterval); 
    _timerControl.reset();

    // Start up the temperature library
    _sensors->begin();
    
    // disable wait timer to remove extra delays. We will wait with a dedicated timer
    _sensors->setWaitForConversion(false);
    _timerConversion.setInterval(750); // equal to #define MAX_CONVERSION_TIMEOUT		750     in DallesTemperature.cpp
    _timerControl.reset();
    
    _sensors->requestTemperatures(); // Send the command to get temperatures

    // initialize health check
    _lastUpdate.setInterval(MAX_TIME_SINCE_LAST_CHANGE);
    _lastUpdate.reset();
    
    
    // locate devices on the bus
    DBG("Locating devices...");
    DBG("Found ");
    DBG(_sensors->getDeviceCount());
    DBGLN(" devices.");
    
};

TemperatureDifference::~TemperatureDifference()
{
    delete _oneWire;
    delete _sensors;
}

String TemperatureDifference::ToString()
{
    String s = "TemperatureDifference:\t";
    s += _name;
    s += "\n_auto\t";
    s += (_auto.value() ? "true": "false");

    s += "\n_curTempDiff\t";
    s += _curTempDiff;

    s += "\n_paramPeriodic\t";
    s += _paramPeriodic;

    s += "\n_paramInterval\t";
    s += _paramInterval;
    
    s += "\n_paramDelta\t";
    s += _paramDelta;

    return s;
};

void TemperatureDifference::onAuto(GroupObject& o)
{
    DBG(GetName());
    DBGLN("--TemperatureDifference: AUTO received");

    bool val = o.value(); 
    
    if (val == 0) // OFF
    {
        DBGLN("\tAuto=OFF, PUMP=OFF");
        _autoControl = false;
        trace("AUTO+PUMP OFF");
        _pump.value(false);
    }   
    else
    {
        DBGLN("\tAuto=ON");
        trace("AUTO ON");
        _autoControl = true;
    }

    #ifdef PERSIST_TO_EEPROM
    // store to EEPROM
    _eepromRecord.auto_flag = val;
    EEPROM.put(EEPROM_BEGIN, _eepromRecord);
    #endif

};


void TemperatureDifference::statusUpdateTemperature()
{
    DBG(GetName());
    DBG("--TemperatureDifference: Sending updated temp difference: ");
    DBGLN(_curTempDiff);

    trace("STATUS DIFF");
    
    _tdiff.value(_curTempDiff);
    _t1.value(_curTemp1);
    _t2.value(_curTemp2);

    // reset timer    
    _timerStatusUpdate.reset();

};

bool TemperatureDifference::IsHealthy()
{
    if (_lastUpdate.isReady())
    {
        trace("DIFF TIMEOUT");
        return false;
    }
    else
    {
        return true;
    }
    // false only if the timer expired (no update since a long while)
}


void TemperatureDifference::setTemp(float diff, float t1, float t2)
{
    _curTempDiff = diff;
    _curTemp1 = t1;
    _curTemp2 = t2;
    
    
    // reset health check timer
    _lastUpdate.reset();

}

void TemperatureDifference::UpdateState()
{

    bool hasError = false;

    if (_timerConversion.isReady())
    {
        // get temps
        float tempA = _sensors->getTempCByIndex(0);
        float tempB = _sensors->getTempCByIndex(1);
        float diff = abs(tempA - tempB);
        

        if (tempA == DEVICE_DISCONNECTED_C || tempB == DEVICE_DISCONNECTED_C)
        {
            trace("SENS MISS DIFF");

            //DBGLN("--TemperatureDifference: ERROR Temperature sensor disconnected.");
            // indicate error via blink code
            if (_timerError.isReady())
            {
                digitalWrite(AUTO_LED, !digitalRead(AUTO_LED));
                _timerError.reset();
                hasError = true;
            }

            // pretend that there is no difference to avoid continuously running the pump
            diff = 0.0f;

        }
        else
        {
            DBG("--TemperatureDifference: Difference measured: ");
            DBGLN(diff);
        }
        
        setTemp(diff, tempA, tempB);
        

        // requestb new temperature readout
        _sensors->requestTemperatures(); // Send the command to get temperatures
        _timerConversion.reset();

    }
    
    if (!hasError)
    {
        if (_autoControl)
        {
            // switch LED on
            digitalWrite(AUTO_LED, HIGH);
        }
        else
        {
            // switch LED on
            digitalWrite(AUTO_LED, LOW);
        }
    }

    if (_timerControl.isReady())
    {
        
        // check if we are outside of the target range
        if (_curTempDiff > _paramMaxDelta)
        {
            // check if pump control is enabled
            if (_autoControl)
            {
                trace("PUMP ON");
                // switch the pump on
                _pump.value(true);
                DBGLN("--TemperatureDifference: Switch Pump ON"); 
                
                // send status update
                statusUpdateTemperature();
            }        
        }

        // check if we have reached the target range
        if (_curTempDiff <= _paramDelta)
        {
            // check if pump control is enabled
            if (_autoControl)
            {
                trace("PUMP OFF");
                // switch the pump off
                _pump.value(false);
                DBGLN("--TemperatureDifference: Switch Pump OFF");
                
                // send status update
                statusUpdateTemperature();
            }        
        }

        _timerControl.reset();
    }
    

    // now, we handle the time-based events if periodic reporting is enabled
    if (_paramPeriodic && _timerStatusUpdate.isReady())
    {
        statusUpdateTemperature();
    }

    
};