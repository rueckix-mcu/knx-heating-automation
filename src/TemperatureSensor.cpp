#include "TemperatureSensor.h"




TemperatureSensor::TemperatureSensor(String name, GroupObject&  goTemp, uint32_t paramInterval, KNXTrace *trace)
        : _name(name), _temp(goTemp), _oneWire(new OneWire(ONE_WIRE_BUS_ABS)), _trace(trace), _sensor(new DallasTemperature(_oneWire))
{
   
    _temp.dataPointType(DPT_Value_Temp);

    // initialize
    
    _curTemp = 0.0f;
    _temp.value(_curTemp);
    
    
    _paramInterval = paramInterval * 1000; //convert to ms
    
    _timerStatusUpdate.setInterval(_paramInterval);
    _timerStatusUpdate.reset();

    _timerError.setInterval(DEFAULT_ERROR_INTERVAL); // error blink code at 1 Hz
    _timerError.reset();


    // Start up the temperature library
    _sensor->begin();
    
    // disable wait timer to remove extra delays. We will wait with a dedicated timer
    _sensor->setWaitForConversion(false);
    _timerConversion.setInterval(750); // equal to #define MAX_CONVERSION_TIMEOUT		750     in DallesTemperature.cpp

    
    _sensor->requestTemperatures(); // Send the command to get temperatures

    // initialize health check
    _lastUpdate.setInterval(MAX_TIME_SINCE_LAST_CHANGE);
    _lastUpdate.reset();
    
    
    // locate devices on the bus
    DBG("Locating devices...");
    DBG("Found ");
    DBG(_sensors->getDeviceCount());
    DBGLN(" devices.");    
};

TemperatureSensor::~TemperatureSensor()
{
    delete _oneWire;
    delete _sensor;
}

String TemperatureSensor::ToString()
{
    String s = "TemperatureSensor:\t";
    s += _name;

    s += "\n_curTemp\t";
    s += _curTemp;

    s += "\n_paramPeriodic\t";
    s += _paramPeriodic;

    s += "\n_paramInterval\t";
    s += _paramInterval;
    
    return s;
};



void TemperatureSensor::statusUpdateTemperature()
{
    DBG(GetName());
    DBG("--TemperatureSensor: Sending updated temp measurement: ");
    DBGLN(_curTempDiff);

    trace("STATUS ABS");
    _temp.value(_curTemp);
    
    // reset timer    
    _timerStatusUpdate.reset();

};

bool TemperatureSensor::IsHealthy()
{
    
    if (_lastUpdate.isReady())
    {
        trace("ABS TIMEOUT");
        return false;
    }
    else
    {
        return true;
    }


    // false only if the timer expired (no update since a long while)
}


void TemperatureSensor::setTemp(float temp)
{
    _curTemp = temp;
    
    
    // reset health check timer
    _lastUpdate.reset();

}

void TemperatureSensor::UpdateState()
{

    if (_timerConversion.isReady())
    {
        // get temps
        float temp = _sensor->getTempCByIndex(0);
        

        if (temp == DEVICE_DISCONNECTED_C)
        {
            trace("SENS MISS ABS");
            //DBGLN("--TemperatureDifference: ERROR Temperature sensor disconnected.");
            // indicate error via blink code
            if (_timerError.isReady())
            {
                digitalWrite(AUTO_LED, !digitalRead(AUTO_LED));
                _timerError.reset();
            }
        }
        else
        {
            DBG("--TemperatureSensor: Measured: ");
            DBGLN(temp);
        }
        
        setTemp(temp);

        // request new temperature readout
        _sensor->requestTemperatures(); // Send the command to get temperatures
        _timerConversion.reset();

    }
  
    // now, we handle the time-based events if periodic reporting is enabled
    if (_timerStatusUpdate.isReady())
    {
        statusUpdateTemperature();
    }

    
};