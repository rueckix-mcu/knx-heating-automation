#include <knx.h>

#include <vector>

#include "platform.h"

#include "TemperatureDifference.h"
#include "TemperatureSensor.h"
#include "KNXTrace.h"

#include <SimpleTimer.h>



#define PARAM_PERIODIC_DIFF 0
#define PARAM_INTERVAL_DIFF PARAM_PERIODIC_DIFF + 1
#define PARAM_DELTA PARAM_INTERVAL_DIFF + 4
#define PARAM_MAX_DELTA PARAM_DELTA + 4
#define PARAM_CONTROL_INTERVAL PARAM_MAX_DELTA + 4
#define PARAM_INTERVAL_ABS PARAM_CONTROL_INTERVAL + 4

// Group Object starting references
#define GO_AUTO 1
#define GO_PUMP 2
#define GO_DIFF 3
#define GO_T1 4 // warm water circulation position 1 (could be in or out, we don't know - they are on the same 1Wire bus)
#define GO_T2 5 // warm water circulation position 2  (could be in or out, we don't know - they are on the same 1Wire bus)
#define GO_TEMP 6 // warm water storage (separate 1Wire bus)
#define GO_TRACE 7 // puts the device into debug trace mode
#define GO_TRACE_MSG 8 // output GO for debug trace messages 14 characters DPT_STRING_ASCII


// Device bags for event processing recurring events
std::vector<IKNXDevice *> devices;
SimpleTimer startupTimer;

#define STARTUP_TIMER 60 * 1000 //60 seconds


/**
 * @brief Create a Com Objects object
 * 
 */
void createComObjects()
{

    KNXTrace *trace = NULL;
    trace = new KNXTrace("KNX Debug Trace",
                            knx.getGroupObject(GO_TRACE),
                            knx.getGroupObject(GO_TRACE_MSG));

   
    devices.push_back(trace);
    DBGLN(trace->ToString().c_str());   

    TemperatureDifference *tempcontrol = NULL;

    tempcontrol = new TemperatureDifference("Warmwasser Steuerung",
                                            knx.getGroupObject(GO_AUTO), // AUTO
                                            knx.getGroupObject(GO_PUMP), //  PUMP
                                            knx.getGroupObject(GO_DIFF), // DIFF
                                            knx.getGroupObject(GO_T1), // Temp 1
                                            knx.getGroupObject(GO_T2), // Temp 2
                                            knx.paramByte(PARAM_PERIODIC_DIFF),
                                            knx.paramInt(PARAM_INTERVAL_DIFF),
                                            knx.paramInt(PARAM_DELTA),
                                            knx.paramInt(PARAM_MAX_DELTA),
                                            knx.paramInt(PARAM_CONTROL_INTERVAL),
                                            trace);


    devices.push_back(tempcontrol);

    DBGLN(tempcontrol->ToString().c_str());


    TemperatureSensor *tempsensor = NULL;
    tempsensor = new TemperatureSensor("Warmwasserspeicher",
                                        knx.getGroupObject(GO_TEMP),
                                        knx.paramInt(PARAM_INTERVAL_ABS),
                                        trace);

    devices.push_back(tempsensor);
    DBGLN(tempsensor->ToString().c_str());
}

bool isHealthy()
{
    if (!startupTimer.isReady() || knx.progMode())
        return true; // ensure minimum uptime for programming and during prog mode

    // UART problem
    if (!knx.bau().enabled())
    {
        blinkCode(ERROR_CODE_BAU);
        return false;
    }

    // KNX device problem
    for (auto const &e : devices)
    {
        if (!e->IsHealthy())
        {
            blinkCode(ERROR_CODE_DEVICE_HEALTH);
            return false;
        }
    }

    return true;
}

/**
 * @brief 
 * 
 */
void setup()
{
    DEBUG_SERIAL.begin(9600);
    ArduinoPlatform::SerialDebug = &DEBUG_SERIAL;

    DBGLN("Booting");

    // check if we are returning from watchdog reset
    checkWatchdog();

    // wait 10 seconds for programming upon startup. Safety delay because we will be putting USB to sleep later
    delay(10000);

    blinkCode(ERROR_CODE_START);

    initPowerManager();

    enableWatchdog();

    startupTimer.setInterval(STARTUP_TIMER);

    randomSeed(millis());

    // for debugging EEPROM issues
    /* 
    uint8_t *buf = knx.platform().getEepromBuffer(KNX_FLASH_SIZE);
    printHex("", buf, KNX_FLASH_SIZE);
    */

    DBGLN("Reading Memory");
    knx.readMemory();
    DBGLN("Memory read");

    // print values of parameters if device is already configured
    if (knx.configured())
    {
        DBGLN("Device configured.");
        createComObjects();
    }
    else
    {
        DBGLN("Device not configured.");
        blinkCode(ERROR_CODE_NOT_CONFIGURED);
    }

    // pin or GPIO the programming led is connected to. Default is LED_BUILTIN
    knx.ledPin(LED_PROG);
    // is the led active on HIGH or low? Default is LOW
    knx.ledPinActiveOn(LED_PROG_ACTIVE);

    // pin or GPIO programming button is connected to A5. Default is 0, which does not work on SAMD21
    knx.buttonPin(BTN_PROG_PIN);
    

    // ensure that we can be reached if there is no address yet.
    if (knx.individualAddress() == 0)
    {
        knx.bau().deviceObject().individualAddress(1); //65535
    } 

    // start the framework.

    DBGLN("Starting KNX");
    knx.start();

    DBGLN("KNX Started");

    //Serial.end();
    //SerialUSB.end();
}

/**
 * @brief Update internal state of devices that need to periodically send bus messages or update internal states
 * 
 */
void KNXDeviceStateUpdate()
{
    for (auto const &e : devices)
    {
        e->UpdateState();
    }
}

void loop()
{
    // don't delay here to much. Otherwise you might lose packages or mess up the timing with ETS
    knx.loop();

    // restart watchdog timer
    resetWatchdog();

    // only run the application code if the device was configured with ETS
    if (knx.configured())
    {
        // trigger periodic state update
        KNXDeviceStateUpdate();

        if (!isHealthy())
        {
            // reset if unhealthy
            NVIC_SystemReset();
        }
    }

    // send to nap regularly so that we save power
    nap();
}