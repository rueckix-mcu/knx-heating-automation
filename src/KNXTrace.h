#pragma once


#include "KNXDevice.h"
#include <SimpleTimer.h>

// delay between trace messages to avoid bus overload
#define DEFAULT_ERROR_INTERVAL 1000
#define MAX_LEN 14

/**
 * @brief KNXTrace allows for in-bus trace debug messages
 * 
 */
class KNXTrace: public IKNXDevice
{

    private:
        String _name;
        GroupObject& _trace;
        GroupObject& _msg;
        

        SimpleTimer _timerStatusUpdate;
        
        char _curMsg[15];

        bool _enabled;
        
    public:
        /**
         * @brief Construct a new temperature sensor object
         * 
         * @param name          Friendly name of the object
         * @param goTrace      Flag enabling trace messages [IN] 
         * @param goMsg         trace message [OUT]
         */
        KNXTrace(String name, GroupObject& goTrace, GroupObject& goMsg);
        ~KNXTrace();
    /**
     * @brief Helper functions
     * 
     */
    public: 
        virtual String ToString();
        virtual void UpdateState();
        virtual bool IsHealthy();

        virtual const char* GetName() {return _name.c_str();};

        /**
         * @brief Update the internal temperature difference
         * 
         * @param msg - trace message. At most 14 characters are supported.
         */
        void SetMsg(const char* msg);


    /**
     * @brief Event handler
     * 
     */
    protected:       

        /**
         * @brief Handler for Enable event (KNX Group Object)
         * 
         * @param o 
         */
        void onEnable(GroupObject& o);
        

        /**
         * @brief Periodically sends the latest message. Note that only the latest message will be printed. No backlog.
         * 
         */
        void statusUpdateMessage();




};









