#pragma once


#include "KNXDevice.h"
#include <SimpleTimer.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#include <KNXTrace.h>

// controls LED blink frequency in case of sensor error
#define DEFAULT_ERROR_INTERVAL 1000

#define MAX_TIME_SINCE_LAST_CHANGE 60*1000 // time in millis (60 seconds)

#ifdef PERSIST_TO_EEPROM
#include <EEPROM.h>

#define MAGICCODE 42
#define EEPROM_BEGIN KNX_FLASH_SIZE // at the end of the KNX program

typedef struct EEPROMRecord
{
    uint8_t magic = 0;
    bool    auto_flag = false;
} record_t;

#endif

class TemperatureDifference: public IKNXDevice
{

    private:
        String _name;
        GroupObject& _auto;
        GroupObject& _pump;
        GroupObject& _tdiff;
        GroupObject& _t1;
        GroupObject& _t2;
        

        OneWire* _oneWire;
        DallasTemperature* _sensors;

        SimpleTimer _timerStatusUpdate;
        SimpleTimer _timerError;
        SimpleTimer _timerControl;
        SimpleTimer _timerConversion;

        bool _autoControl;
        float _curTempDiff;
        float _curTemp1;
        float _curTemp2;

        #ifdef PERSIST_TO_EEPROM
        EEPROMRecord _eepromRecord;
        #endif

        bool _paramPeriodic;
        uint32_t _paramInterval; // runtime parameter in milliseconds
        uint32_t _paramControlInterval; // runtime param in millis
        uint32_t _paramDelta;
        uint32_t _paramMaxDelta;
        
        SimpleTimer _lastUpdate;

        KNXTrace *_trace = NULL;


    public:
        /**
         * @brief Construct a temperature difference object
         * 
         * @param name      Friendly name of the object
         * @param goAuto    KNX group object for Automatic pump contorl     [IN]
         * @param goTempDiff  KNX group object for temperatuir difference   [OUT]
         * @param goPump    KNX group object for on/off control of water pump [OUT]
         * @param goT1      First temperature sensor value [OUT]
         * @param goT2      Secondtemperature sensor value [OUT]
         * @param paramPeriodic  Parameter controlling the periodic sending of temp differences (yes=1/no=0)
         * @param paramInterval Paramter controlling the sending interval (in 1s) for periodic updates
         * @param paramDelta    Parameter controlling the target temperature difference (in 1°K). 
         * @param paramMaXDelta    Parameter controlling the max temperature difference (in 1°K). 
         * @param paramControlInterval    Parameter controlling the minimum time between pump on-off signals in automatick mode (in sec)
         * @param trace     debug tracer (optional)
         */
        TemperatureDifference(String name, GroupObject& goAuto, GroupObject&  goTempDiff, GroupObject&  goPump, GroupObject& goT1, GroupObject& goT2, uint8_t paramPeriodic, uint32_t paramInterval, uint32_t paramDelta, uint32_t paramMaxDelta, uint32_t paramControlInterval, KNXTrace *trace);
        ~TemperatureDifference();
    /**
     * @brief Helper functions
     * 
     */
    public: 
        virtual String ToString();
        virtual void UpdateState();
        virtual bool IsHealthy();

        virtual const char* GetName() {return _name.c_str();};


    /**
     * @brief Event handler
     * 
     */
    protected:
        /**
         * @brief Handler for Auto event (KNX Group Object)
         * 
         * @param o 
         */
        void onAuto(GroupObject& o);
        

        /**
         * @brief Update the internal temperature difference
         * 
         * @param diff - new temperature difference
         * @param t1    - new temperature 1
         * @param t2    - new temperature 2
         */
        void setTemp(float diff, float t1, float t2);
        

        /**
         * @brief Periodically sends current temperature difference. If enabled by ETS param.
         * 
         */
        void statusUpdateTemperature();

        /**
         * @brief set trace message
         * 
         * @param msg 
         */
        void trace(const char* msg)
        {
            if (_trace)
                _trace->SetMsg(msg);
        }



};









